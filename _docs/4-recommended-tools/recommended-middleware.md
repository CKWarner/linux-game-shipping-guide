---
title: "Middleware"
category: "Recommended Tools"
order: 2
---

## SDL

Quoting the official website:

>Simple DirectMedia Layer is a cross-platform development library designed to
provide low level access to audio, keyboard, mouse,  joystick, and graphics
hardware via OpenGL and Direct3D. It is used by video playback software,
emulators, and popular games including [Valve](http://valvesoftware.com/)'s
award winning catalog and many [Humble Bundle](https://www.humblebundle.com/)
games.

SDL is a library commonly used to simplify the work of creating cross platform
games or game engines, and supports Windows, Mac OS X, Linux, iOS, and Android.
SDL has been in development for a long time and is a highly effective solution
for abstracting the details of how to perform common OS specific tasks. This is
especially useful for porting a game to Linux with an existing codebase written
from scratch.

For more information on how to use SDL, [consult the SDL's official wiki.](https://wiki.libsdl.org/FrontPage)
