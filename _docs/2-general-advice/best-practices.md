---
title: "Best Practices"
category: "General Advice"
order: 3
---

## Where To Store Game Data

The [XDG Base Directory Specification][2] in simple terms describes where your
software should place files on a user's Linux PC. Much like on Windows, game
configuration data and save game data should be stored in the appropriate
directories to avoid cluttering your user's `/home` directory. Use
`$XDG_DATA_HOME` for savegames and `$XDG_CONFIG_HOME` for configuration files.

> **Note:** Your game's code shouldn't assume that these values are always set and
available, always check if these values are set, and fall back to safe values
if they are not.

> **Note:** The game engine Unity stores all game data under: `~/.config/unity3d/[Your Game]`
> This doesn't adhere to the XDG Base Directory Specification, but is still
acceptable and better than cluttering the user's `/home` directory.

> **Note:** On Linux, the **~** character in a filepath refers to the user's 'home'
directory. For a user with the username `bob`, the filepath `~/file` would refer
to `/home/bob/file`.

## 64-bit Only

While there's no reason why you can't ship a 32-bit version of your game, it is
very unnecessary and not an additional burden you need to carry.

32-bit support is increasingly being phased out of the world of computing, and
this is true on Linux as well. [The number of 32-bit Linux installations][1] is
now so low that there is no benefit to choosing 32-bit over 64bit, or
supporting 32-bit alongside 64-bit.

## Locale Handling

_TODO. Need more information on this, including how to obtain locale
information for current language, keyboard input, timezone, etc, in native C++
applications, as well as in popular game engines._

[1]: https://www.gamingonlinux.com/users/statistics
[2]: https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
