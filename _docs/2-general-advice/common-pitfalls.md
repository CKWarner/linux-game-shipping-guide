---
title: "Common Pitfalls"
category: "General Advice"
order: 4
---

## Don't Mix Crossplatform Engines With Platform Dependent Middleware

A game engine like Unreal Engine, Unity or Godot gives you incredible
flexibility and portability, until you use Windows-only Middleware, locking you
to only one platform. Be mindful of what middleware you use to create your game
and always aim for cross platform middleware.

This advice applies to game development in general, not just for titles planning
to support Linux, as you never know which future platforms you may wish to
support.

## Filepath Capitalisation

It's such a small detail that it's easy to overlook. Windows filepaths are not
case sensitive. Linux filepaths are case sensitive.

This can create relatively minor issues that can break games when porting them
to Linux. The general advice is to pick a filepath capitalisation structure and
stick to it, or simply use lowercase for everything.

## Open File Formats for Audio/Video

For maximum flexibility to port your game to every platform, it is strongly
advised to use open file formats for audio and video. Otherwise you might find
yourself needing to re-encode all of your video/audio assets for a new platform.

## Executable Permission

Application executable files on Linux must have executable permission enabled
before shipping, otherwise they will not run. Whether you are compiling a game
yourself from C++, or exporting a build of your game from a game engine's
editor, always check that the permission has been enabled. On Linux it is
possible to check this in most distributions by simply right clicking the file
in your file manager and viewing it's properties.

If your game files are going to be compressed and then uncompressed during
installation on your user's PC, ensure the file permissions are retained during
the whole process.
